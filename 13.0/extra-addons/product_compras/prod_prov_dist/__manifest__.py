# -*- coding: utf-8 -*-
{
    'name': "Productos con Proveedor y Distribuidor",

    'summary': """
        Agrega el proveedor y distribuidor en el formulario de Productos
    """,

    'description': """
         Agrega el proveedor y distribuidor en el formulario de Productos
    """,

    'author': "Fco. Santa",
    'website': "http://www.yourcompany.com",


    'category': 'Sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['product'],


    'data': [
        'views/product_view.xml'
    ],

}
