# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    # obtiene el precio mínimo de compra
    def _compute_precio_kilo_litro(self):
        for r in self:
            precio_minimo = 9999
            for seller_id in self.seller_ids:
                if seller_id['price'] < precio_minimo:
                    r.precio_kilo_litro = seller_id['price']
                    precio_minimo = seller_id['price']

        if precio_minimo == 9999:
            r.precio_kilo_litro = 0

    #definición de los campos
    space_bk = fields.Char(' ', readonly=True)
    proveedor = fields.Many2one('res.partner', string='Proveedor',
                                required=False,
                                domain=[('is_company', '=', True)])
    distribuidor = fields.Many2one('res.partner', string="Distribuidor",
                                   required=False,
                                   domain=[('is_company', '=', True)])
    clasifica_supercoop = fields.Char('Clasificación Supercoop')
    peso_vol_unidad = fields.Float(
        'Peso o volumen de la unidad (gramos o mililitros)')
    precio_kilo_litro = fields.Float(compute=_compute_precio_kilo_litro,
                                     string='Precio por kilo o litro')
